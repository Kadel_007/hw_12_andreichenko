# Напишите декоратор, который бы сообщал о времени выполнения функции
# Напишите переметризованный декоратор,
# который бы выводил время выполнения функции в милисекундах или секундах или минутах
# (как выводить - определяет параметр декоратора)

import time


def time_of_function():
    a = 0
    for i in range(1000):
        a = i
    return "All is good"


# I
# def time_decorator(func):
#     def _wrapper():
#         start_time = time.time()
#         res = func()
#         end_time = time.time() - start_time
#         print("Time of executing of function in seconds", end_time)
#         return res
#
#     return _wrapper
#
#
# time_of_function = time_decorator(time_of_function)
# print(time_of_function())

# II
def parameter_time_decorator(time_type: str, func):
    def _wrapper():
        if isinstance(time_type, str):
            start_time = time.time()
            res = func()
            if time_type == "seconds":
                end_time = time.time() - start_time
            elif time_type == "milliseconds":
                end_time = (time.time() - start_time) * 1000
            elif time_type == "minutes":
                end_time = (time.time() - start_time) / 60
            elif time_type == "hours":
                end_time = (time.time() - start_time) / 3600
            else:
                end_time = 'Unknown key...'
            print(f"Time of execution of function in {time_type}: {end_time}")
            return res
        else:
            raise TypeError

    return _wrapper


time_type = "milliseconds"  # milliseconds| seconds | minutes | hours
time_of_function = parameter_time_decorator(time_type, time_of_function)
print(time_of_function())